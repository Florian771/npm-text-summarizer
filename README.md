# nodejs-text-summarizer logger bug error workaround: natural update to latest version

# install

yarn add bitbucket:Florian771/npm-text-summarizer

# usage

import \* as summarizer from "npm-text-summarizer"

interface TextSummarizerOptions {
n: number,
lang: string,
raw: boolean
}

const text: string = "Es war kein gewöhnlicher Spätsommer, jener vor drei Jahren. Am Wiener West- und Hauptbahnhof herrschte Ausnahmezustand. In Österreich stellen damals über 87.600 Schutzsuchende einen Asylantrag. Menschen aus Syrien, Afghanistan, dem Irak kamen nach langer, oftmals beschwerlicher Reise in Wien an. Teilweise mehr als 6.000 an einem Tag."

const opt: TextSummarizerOptions = {
n: 1,
lang: language ? language : "DE",
raw: true
}

const result: string = await summarizer(text, opt)
